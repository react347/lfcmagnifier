import './App.css';

import Header from './Header/Header';
import SimpleMagnifier from './SimpleMagnifier/SimpleMagnifier';

function App() {
  return (
    <div className="App">
      <Header/>
      <SimpleMagnifier/>
    </div>
  );
}

export default App;
