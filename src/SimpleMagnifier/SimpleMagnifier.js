import React, { useRef, useEffect } from 'react';
import "./SimpleMagnifier.css";
import {RIMagnifier} from '@lylech/react-image-magnifier';

import img1 from "../img/bmw01-01.jpeg";
import img2 from "../img/bmw01-02.jpeg";
import img3 from "../img/bmw01-03.jpeg";
import img4 from "../img/bmw01-04.jpeg";
import img5 from "../img/bmw01-05.jpeg";

import img6 from "../img/img-uns-01.jpg";
import img7 from "../img/img-uns-02.jpg";
import img8 from "../img/img-uns-03.jpg";
import img9 from "../img/img-uns-04.jpg";
import img10 from "../img/img-uns-05.jpg";

import imgcd1 from "../img/img-cd1.jpg";
import imgcd2 from "../img/img-cd2.jpg";
import imgcd3 from "../img/img-cd3.jpg";
import imgcd4 from "../img/img-cd4.jpg";
import imgcd5 from "../img/img-cd5.jpg";
import imgcd6 from "../img/img-cd6.jpg";
import imgcd7 from "../img/img-cd7.jpg";
import imgcd8 from "../img/img-cd8.jpg";
import imgcd9 from "../img/img-cd9.jpg";

export default function LfcMagnifier(props) {


  // Handles to the magnifier instances
  const rim01 = useRef(null);
  const rim02 = useRef(null);

  // Handles to the containers
  const refDemo01 = useRef(null);
  const refDemo02 = useRef(null);
  const refDemo03 = useRef(null);

  const onThumbnailClick=(idxImage)=>{
    console.log("@ onThumbnailClick: Clicked on image# %o.", idxImage);
  };

  useEffect( ()=>{
    const images = [img9, img10, img1, img2, img3, img4, img5, img6, img7, img8];
    const zooms =[   4,    4,   2.5,  2.5,    2,  1.5];
    rim01.current = new RIMagnifier({container: refDemo01.current, arrayOfImagePaths: images,arrayOfImageZoomFactors: zooms,fnClickHandler: onThumbnailClick });
    rim02.current = new RIMagnifier({container: refDemo02.current, arrayOfImagePaths: images,arrayOfImageZoomFactors: zooms, initialSelection:7 });
    new RIMagnifier({container: refDemo03.current,
      arrayOfImagePaths: [imgcd1, imgcd2, imgcd3, imgcd4, imgcd5, imgcd6, imgcd7, imgcd8, imgcd9],
    });
  });

  /* Direct DOM manipulation to prevent React from refreshing */
  const clickMenuOnTop=()=>{window.document.getElementById("demo01").className = "my-container-demo01 ribbon-top";}
  const clickMenuOnBottom=()=>{window.document.getElementById("demo01").className = "my-container-demo01";}
  const clickMenuOnLeft=()=>{window.document.getElementById("demo01").className = "my-container-demo01 ribbon-left";}
  const clickMenuOnRight=()=>{window.document.getElementById("demo01").className = "my-container-demo01 ribbon-right";}

  const clickToggleThumbnails=()=>{window.document.getElementById("demo02").classList.toggle("nothumbnails");}

  return (
    <>
    <div className="my-container-demo01" id="demo01">
      <div className="my-text-bar">
        <h3>Interactive Demo: Introduction</h3>
        <ul>
          <li>This widget is an out-of-the-box solution for showcasing images in a sleek and user-friendly way.</li>
          <li>Users can easily inspect and magnify images by moving their mouse over the thumbnails and clicking to select.</li>
          <li>Responsive: So it can be used on different devices and screen sizes.</li>
        </ul>

        <h3>E.g.#1: Positioning the Thumbnail Ribbon</h3>
        <ul>
          <li>The thumbnail ribbon may be positioned along any edge of the container.</li>
          <li>Click on the buttons to explore the different layouts.</li>
          <li className="nodot"><button className={"rim-button"} onClick={(e)=>{clickMenuOnTop()}} >Show Thumbnails on Top</button></li>
          <li className="nodot"><button className={"rim-button"} onClick={(e)=>{clickMenuOnBottom()}} >Show Thumbnails on Bottom</button></li>
          <li className="nodot"><button className={"rim-button"} onClick={(e)=>{clickMenuOnLeft()}} >Show Thumbnails on Left</button></li>
          <li className="nodot"><button className={"rim-button"} onClick={(e)=>{clickMenuOnRight()}} >Show Thumbnails on Right</button></li>
        </ul>

      </div>
      <div className='react-image-magnifier' ref={refDemo01} />
    </div>

    <div className="my-container-demo02" id="demo02">
      <button className={"rim-button"} onClick={(e)=>{rim02.current.previousImage();}}>&lt; Previous</button>
      <div className='react-image-magnifier' ref={refDemo02} />
      <button className={"rim-button"} onClick={(e)=>{rim02.current.nextImage()}}>Next &gt;</button>
      <div className="my-text-bar">
        <h3>E.g.#2: Previous / Next buttons</h3>
        <ul>
          <li>The widget exposes functionality to step through images programatically.</li>
          <li>These serve as an alternative to using the thumbnail ribbon for image selection.</li>
          <li>This makes it possible to hide the thumbnail ribbon completely.</li>
          <li>In this example, the Previous and Next buttons invoke <pre>.previousImage()</pre> and <pre>.nextImage()</pre> respectively.</li>
        </ul>
        <ul>
          <li>We may also choose to hide the thumbnails altogether.</li>
          <li className="nodot"><button className={"rim-button"} onClick={(e)=>{clickToggleThumbnails()}} >Toggle Thumbnails</button></li>
        </ul>
      </div>
    </div>

    <div className="my-container-demo03">
     <div className="my-text-bar">
        <h3>E.g.#3: Customizable Layouts</h3>
        <ul>
          <li>By design of the widget is fully customizable, allowing users to apply their own styles to the containers.</li>
          <li>This makes it easy to create unique and visually appealing layouts that match the theme of the website or application.</li>
          <li>The design of the widget readily exposes the container CSS classes, making it easy to add user defined styles to the containers.</li>
          <li>As an example, here is a circular layout reminiscient of a Lazy Susan, suitable perhaps for a restaurant menu theme.</li>
        </ul>
        <ul>
        <li className='nodot'>Hungry?</li>
        </ul>
      </div>
      <div className='react-image-magnifier' ref={refDemo03} />
    </div>

   </>
  )
}